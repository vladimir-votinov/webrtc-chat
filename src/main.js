// DOM
const sendBtn = document.querySelector("#send");
const fileInp = document.querySelector("#fileInput");
const fileBtn = document.querySelector("#sendFile");
const chatLog = document.querySelector("#chat");
const message = document.querySelector("#message");
const chatLink = document.querySelector("#chatLink");
const usersList = document.querySelector("#users");
const connectionNumber = document.querySelector("#connectionNumber");

// WebRTC
let peers = {};
let server = {
    urls: [
        "stun:stun.l.google.com:19302",
    ]
};
let options = {};

// Cross-browser
let PeerConnection = window.RTCPeerConnection;
let SessionDescription = window.RTCSessionDescription;
let IceCandidate = window.RTCIceCandidate;

let receiveBuffer = [];
let receivedSize = 0;
let receiveSize = 0;
let receiveName = '';

const uuid = () => {
    var s4 = function() {
        return Math.floor(Math.random() * 0x10000).toString(16);
    };
    return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
};
const nick = prompt("What your name?");
const socket = io.connect("localhost:3000", {"sync disconnect on unload": true});
const sendDSL = (type, message, to) => {
    socket.emit("webrtc", JSON.stringify({id: id, name: nick, to: to, type: type, data: message}));
};
const socketReceived = data => {
    let json = JSON.parse(data);
    switch (json.type) {
        case "candidate":
            remoteCandidateReceived(json.id, json.data, json.name);
            break;
        case "offer":
            remoteOfferReceived(json.id, json.data, json.name);
            break;
        case "answer":
            remoteAnswerReceived(json.id, json.data);
            break;
    }
};
const socketNewPeer = data => {
    usersList.innerHTML += "<div>" + data.name + "</div>";
    peers[data.id] = {
        candidateCache: [],
        name: data.name
    };

    renderPeers();

    let pc = new PeerConnection(server, options);
    initConnection(pc, data.id, "offer");
    let channel = pc.createDataChannel("dataChannel", {});
    peers[data.id].connection = pc;
    peers[data.id].channel = channel;

    bindEvents(channel, data.id);

    pc.createOffer().then(
        description => pc.setLocalDescription(description)
    ).then(
        () => console.log('setLocalDescription success')
    );
};
const renderPeers = () => {
    usersList.innerHTML = '';

    for (let peer in peers) if (peers.hasOwnProperty(peer)) {
        usersList.innerHTML += '<div>' + peers[peer].name + '</div>';
    }
};
const initConnection = (pc, id, sdpType) => {
    pc.onicecandidate = (event) => {
        if (event.candidate) {
            peers[id].candidateCache.push(event.candidate);
        } else {
            sendDSL(sdpType, pc.localDescription, id);
            for (let i = 0; i < peers[id].candidateCache.length; i++) {
                sendDSL("candidate", peers[id].candidateCache[i], id);
            }
        }
    };
    pc.oniceconnectionstatechange = (event) => {
        if (pc.iceConnectionState === "disconnected") {
            connectionNumber.innerText = parseInt(connectionNumber.innerText) - 1;
            delete peers[id];
            renderPeers();
        }
    };
};
const bindEvents = (channel, peerId) => {
    channel.onopen = () => {
        connectionNumber.innerText = parseInt(connectionNumber.innerText) + 1;
    };
    channel.onmessage = (event) => {
        if (typeof event.data === "object") {
            receiveBuffer.push(event.data);
            receivedSize += event.data.byteLength;

            // TODO: send file metadata via socket
            if (receivedSize >= receiveSize) {
                const received = new Blob(receiveBuffer);
                receiveBuffer = [];
                receivedSize = 0;

                chatLog.innerHTML += "<div>" + peers[peerId].name + " : <a target='_blank' download='" + receiveName + "' href='" + URL.createObjectURL(received) + "'>download</a></div>";
                receiveName = '';
                receiveSize = 0;
            }
        } else {
            chatLog.innerHTML += "<div>" + peers[peerId].name + " : " + event.data + "</div>";
        }
    };
};
const remoteOfferReceived = (id, data, name) => {
    createConnection(id, name);
    let pc = peers[id].connection;

    pc.setRemoteDescription(new SessionDescription(data)).then(
        () => console.log('setRemoteDescription success')
    );

    pc.createAnswer().then(
        description => pc.setLocalDescription(description)
    ).then(
        () => console.log('setLocalDescription success')
    );
};
const createConnection = (id, name) => {
    if (peers[id] === undefined) {
        peers[id] = {
            candidateCache: [],
            name: name
        };
        let pc = new PeerConnection(server, options);
        initConnection(pc, id, "answer");

        peers[id].connection = pc;
        pc.ondatachannel = (event) => {
            peers[id].channel = event.channel;
            peers[id].channel.owner = id;
            bindEvents(peers[id].channel, id);
        }
    }

    renderPeers();
};
const remoteAnswerReceived = (id, data) => {
    let pc = peers[id].connection;
    pc.setRemoteDescription(new SessionDescription(data));
};
const remoteCandidateReceived = (id, data, name) => {
    createConnection(id, name);
    let pc = peers[id].connection;
    pc.addIceCandidate(new IceCandidate(data)).then(
        () => console.log('addIceCandidate success')
    );
};
const sendMessage = () => {
    let msg = message.value;

    for (let peer in peers) {
        if (peers.hasOwnProperty(peer)) {
            if (peers[peer].channel !== undefined) {
                try {
                    peers[peer].channel.send(msg);
                } catch (e) {
                    console.log(e);
                }
            }
        }
    }
    chatLog.innerHTML += '<div class="right-align">Your: ' + msg + '</div>';
    message.value = "";
};
const sendFile = event => {
    let offset = 0;
    const chunkSize = 16384;
    const file = fileInp.files[0];
    const reader = new FileReader();

    socket.emit("file", JSON.stringify({file: {name: file.name, size: file.size}}));

    reader.addEventListener('load', e => {
        // Send chunk by peers
        for (let peer in peers) {
            if (peers.hasOwnProperty(peer)) {
                if (peers[peer].channel !== undefined) {
                    try {
                        peers[peer].channel.send(e.target.result);
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        }

        // Read next chunk
        offset += e.target.result.byteLength;
        if (offset < file.size) {
            readSlice(offset);
        }
    });

    const readSlice = o => {
        const slice = file.slice(offset, o + chunkSize);
        reader.readAsArrayBuffer(slice);
    };
    readSlice(0);
};
const onBeforeUnload = event => {
    for (let peer in peers) {
        if (peers.hasOwnProperty(peer)) {
            if (peers[peer].channel !== undefined) {
                try {
                    peers[peer].channel.close();
                } catch (e) {
                    console.log(e);
                }
            }
        }
    }
};
const socketFileCandidate = mesage => {
    // TODO: Receive file metadata, set local variables
    mesage = JSON.parse(mesage);

    receiveSize = mesage.file.size;
    receiveName = mesage.file.name;
};

let room = location.hash.substr(1);
let id = uuid();

if (!room) {
    room = uuid();
}
chatLink.innerHTML = "<a href='#"+room+"'>Link to the room</a>";

// Subscribe socket event
socket.on("webrtc", socketReceived);
socket.on("new", socketNewPeer);
socket.on("file", socketFileCandidate);
// Start
socket.emit("room", JSON.stringify({id: id, room: room, nick: nick}));

sendBtn.addEventListener("click", sendMessage);
fileBtn.addEventListener("click", sendFile);
window.addEventListener("beforeunload", onBeforeUnload);