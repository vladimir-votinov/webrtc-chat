const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);
let users = {};

app.get("/", function(req, res) {
    res.sendFile(__dirname + "/src/index.html");
});

app.get("/main.js", function(req, res) {
    res.sendFile(__dirname + "/src/main.js");
});

io.on("connection", socket => {
    socket.on("room", message => {
        let json = JSON.parse(message);
        users[json.id] = socket;
        if (socket.room !== undefined) {
            socket.leave(socket.room, err => console.log(err));
        }

        socket.room = json.room;
        socket.join(socket.room);
        socket.user_id = json.id;

        socket.broadcast.to(socket.room).emit("new", {id: json.id, name: json.nick});
    });

    // Сообщение, связанное с WebRTC (SDP offer, SDP answer или ICE candidate)
    socket.on("webrtc", message => {
        let json = JSON.parse(message);
        if (json.to !== undefined && users[json.to] !== undefined) {
            users[json.to].emit("webrtc", message);
        } else {
            socket.broadcast.to(socket.room).emit("webrtc", message);
        }
    });

    socket.on("disconnect", () => {
        socket.broadcast.to(socket.room).emit("leave", socket.user_id);
        delete users[socket.user_id];
    });

    socket.on("file", message => {
        socket.broadcast.to(socket.room).emit("file", message);
    })
});

http.listen(3000, function(){
    console.log("listening on *:3000");
});