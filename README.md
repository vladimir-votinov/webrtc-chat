# WebRTC Chat

Install dependency
```
npm i
```

Run node.js server
```
npm start
```

Open page in browser
```
http://localhost:3000
```